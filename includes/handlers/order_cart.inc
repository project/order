<?php // $Id$

class order_cart {
  var $cid;
  var $uid;
  var $sid;
  var $pfid;
  var $title;
  var $timestamp;
  var $data;

  var $items;
  var $items_extra;

  function access() {
    global $user;
    return (user_access('administer orders') 
      || ($user->uid && user_access('use cart') && ($this->uid == $user->uid)));
  }

  function set_uid($uid = NULL) {
    if (!$uid) {
      global $user;
      $uid = $user->uid;
    }
    $this->uid = (int) $uid;
  }

  function set_sid($sid = NULL) {
    if (!$sid) {
      $sid = session_id();
    }
    $this->sid = $sid;
  }

  function set_pfid($pfid = NULL) {
    $this->pfid = (int) $pfid;
  }

  function set_title($title = NULL) {
    $this->title = check_plain($title);
  }

  function set_timestamp($timestamp = NULL) {
    if (!$timestamp) $timestamp = time();
    $this->timestamp = (int) $timestamp;
  }

  function save() {
    $update = $this->cid ? 'cid' : NULL;
    drupal_write_record('order_cart', $this);
  }

  function delete() {
    db_query("DELETE FROM {order_cart} WHERE cid = %d", $this->cid);
    db_query("DELETE FROM {order_cart_item} WHERE cid = %d", $this->cid);
  }

  function add_item($item) {
  }

  function delete_item($item) {
  }

}
