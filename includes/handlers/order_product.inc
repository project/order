<?php // $Id$

class order_product {
  var $opid;
  var $handler;
  var $nid = 0;
  var $weight = 0;
  var $cost;
  var $qty;
  var $title;
  var $description;
  var $description_format;
  var $data;

  function access() {
  }

  function set_handler() {
    $this->handler = get_class($this);
  }

  function set_status($status = 1) {
    $this->status = (int) (bool) $status;
  }

  function set_nid($nid = 0) {
    $this->nid = (int) $nid;
  }

  function set_weight($weight = 0) {
    $this->weight = (int) $weight;
  }

  function set_cost($cost = 0) {
    $this->cost = (float) $cost;
  }

  function set_qty($cost = 0) {
    $this->qty = (float) $qty;
  }

  function set_title($title = NULL) {
    $this->title = check_plain($title);
  }

  function set_description($description = '') {
    $this->description = filter_xss($description);
  }

  function save() {
    $update = $this->opid ? 'opid' : NULL;
    drupal_write_record('order_product', $this);
  }

  /**
   * For reporting and referential integrity, products can't be deleted.
   * Respond to a delete call by setting it inactive.
   */
  function delete() {
    $this->set_status(FALSE);
    $this->save();
  }
}
