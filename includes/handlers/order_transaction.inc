<?php // $Id$

class order_transaction extends pay_transaction {

  var $items;
  var $costs;
  var $addresses;

  function set_items() {
    $this->items = array();
    $res = db_query("SELECT * FROM {order_transaction_item} 
      WHERE otid = %d ORDER BY type, title", $order->otid);

    while ($row = db_fetch_object($res)) {
      if (!isset($order->items[$row->type])) $order->items[$row->type] = array();
      $this->items[$row->type][] = $row;
    }
  }

  function set_costs() {
    $this->costs = array();
    $res = db_query("SELECT * FROM {order_transaction_cost} WHERE otid = %d", $order->otid);
    while ($row = db_fetch_object($res)) {
      $this->costs[$row->name] = $row->cost;
      $this->order_status = $row->status;
    }
  }

  function set_addresses() {
    $this->address = new stdClass();
    $res = db_query("SELECT * FROM {order_transaction_address} 
      WHERE otid = %d ORDER BY type", $this->pxid);
    while ($row = db_fetch_object($res)) {
      $this->address->{$row->type} = $row;
    }
  }

  function items() {
    return $this->items();
  }

  function costs() {
    return $this->costs();
  }

  function addresses() {
    return $this->addresses();
  }
}
