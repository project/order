<?php // $Id$

function order_cart($cid = NULL) {
  $cart = order_cart_load($cid);
  $cart = order_cart_prepare($cart);
  if (!$cart) return theme('order_cart_empty');

  return drupal_get_form('order_cart_form', $cart);
}

function order_cart_load($cart = NULL) {
  global $user;
  $uid = $user->uid;
  $sid = session_id();

  // protect against getting data for users who aren't supposed to have it
  // I have turned this off so users who are registering can save their carts
  // if we need to protect carts, i think we'll have to find another mechanism
  // --mark
  // if (($uid != $user->uid) && !user_access('administer orders')) return;
  if (is_object($cart)) {

    $fields = array('cid', 'uid', 'sid', 'title', 'stamp', 'items', 'data', 'items_extra');

    $cart->data = array();
    foreach ($cart as $key => $val) {
      if (!in_array($key, $fields)) $cart->data[$key] = $val;
    }
    $cart->data = serialize($cart->data);

    // Create the SQL
    if (user_access('administer orders')) { // honor everything from admins
      $key = 'cid';
      $val = $cart->cid;
    }
    else if ($uid) { // update the cart for a logged in user
      $key = 'uid';
      $val = $uid;
    }
    else { //anonymouse users
      $key = 'sid';
      $val = $sid;
    }

    if ($cid = $cart->cid) { // this cart already exists and needs to be updated
      if (count($cart->items) || count($cart->items_extra)) {
        db_query("UPDATE {order_cart} SET sid = '%s', data = '%s', stamp = %d WHERE %s = %d", $sid, $cart->data, time(), $key, $val);
      }
      else {
        db_query("DELETE FROM {order_cart} WHERE %s = %d", $key, $val);
      }

      db_query("DELETE FROM {order_cart_product} WHERE cid = %d", $cart->cid);
    }
    else { // create a new cart
      db_query("INSERT INTO {order_cart} (cid, uid, sid, data, stamp) VALUES ( %d, %d, '%s', '%s', %d )", $cid, $uid, $sid, $cart->data, time());
      $cid = db_last_insert_id();
    }

    foreach ($cart->items as $nid => $data) {
      if (!$data['qty']) continue;
      db_query("INSERT INTO {order_cart_product} ( cid, opid, name, qty, cost, stamp )
          VALUES ( %d, %d, '%s', %d, %f, %d )", $cid, $data['opid'], $data['name'], $data['qty'], $node->cost, $data['stamp']);
    }
    if (is_array($cart->items_extra)) {
      foreach ($cart->items_extra as $data) {
        db_query("INSERT INTO {order_cart_item} ( cid, name, qty, cost, stamp )
          VALUES ( %d, '%s', %d, %f, %d )", $cid, $data['name'], $data['qty'], $data['cost'], $data['stamp']);
      }
    }
  }

  if (is_int($cart) && user_access('administer orders')) { // trying to load a cart by id
    $res = db_query("SELECT * FROM {order_cart} WHERE cid = %d", $cid);
  }
  else if ($uid) {   // logged in user
    $res = db_query("SELECT * FROM {order_cart} WHERE uid = %d", $uid);
  }
  else {           // anonymous user
    $res = db_query("SELECT * FROM {order_cart} WHERE sid = '%s'", $sid);
  }

  if (!$cart = db_fetch_object($res)) return;

  // initialize some cart values
  $cart->address =  new stdClass();

  if (function_exists('postal_address')) {
    $cart->address->billing = '';
    postal_address($cart->address->billing, 'billing');
    $cart->address->billing->required = FALSE;
    $cart->address->billing->hidden = TRUE;
    $cart->address->billing->title = t('Billing Address');

    $cart->address->shipping = '';
    postal_address($cart->address->shipping, 'shipping');
    $cart->address->shipping->required = FALSE;
    $cart->address->shipping->hidden = TRUE;
    $cart->address->shipping->title = t('Shipping Address');
  }

  $cart->valid = TRUE;

  foreach (unserialize($cart->data) as $key => $data) {
    $cart->$key = $data;
  }
  unset($cart->data);

  $cart->items = array();
  $cart->items_extra = array();

  $res = db_query("SELECT * FROM {order_cart_item} WHERE cid = %d ORDER BY stamp ASC", $cart->cid);
  while ($row = db_fetch_object($res)) {
    if ($row->nid) {
      $cart->items[$row->nid] = array( 'qty' => $row->qty, 'name' => $row->name, 'stamp' => $row->stamp );
    }
    else {
      $cart->items_extra[$row->name] = array( 'qty' => $row->qty, 'cost' => $row->cost, 'stamp' => $row->stamp );
    }
  }
  return $cart;

}

function order_cart_prepare($cart) {
  if (!$cart) return;


  $modules = module_implements('order_cart_alter');

  foreach ($modules as $module) {
    $function = $module .'_order_cart_alter';
    $item = NULL;
    $data = NULL;
    $function('cart', $cart, $item, $data);
    order_cart_costs($cart);
    order_cart_address($cart);
  }

  foreach ($cart->items as $nid => $item) {
    $node = node_load($nid);
    //$item['cost'] = isset($item['cost']) ? $item['cost'] : $node->order_cost;
    $item['cost'] = $node->order_cost;
    $item['node'] = $node;

    $data = NULL;
    foreach ($modules as $module) {
      $function = $module .'_order_cart_alter';
      $function('item', $cart, $item, $data);
      $cart->items[$nid] = $item;
      order_cart_costs($cart);
      order_cart_address($cart);
    }
  }

  $item = NULL;
  $data = NULL;
  foreach ($modules as $module) {
    $function = $module .'_order_cart_alter';
    $function('total', $cart, $item, $data);
    order_cart_costs($cart);
  }

  order_cart_costs($cart);
  order_cart_address($cart);
  order_cart_load($cart);  //save changes
  return $cart;
}

function order_cart_costs(&$cart) {
  $costs = array('subtotal' => 0.00, 'total' => 0.00);

  foreach ($cart->items as $nid => $data) {
    $node = $data['node'];
    $qty = isset($data['qty']) ? $data['qty'] :  1;
    $cost = isset($data['cost']) ? $data['cost'] : ($node->order_cost);
    $cost = $cost * $qty;
    $costs['subtotal'] += $cost;
    $costs['total']    += $cost;
    $cart->items[$nid]['node'] = $node;
  }

  foreach ($cart->items_extra as $name => $data) {
    $qty = isset($data['qty']) ? $data['qty'] : 1;
    $cost = $data['cost'] * $qty;
    $costs[$name] = $cost;
    $costs['total'] += $cost;
  }

  $cart->costs = $costs;

  return $costs;
}

function order_cart_address(&$cart) {
  foreach ($cart->address as $type => $a) {

    if (function_exists('postal_address')) {
      postal_address($cart->address->$type, $a->id, $cart);
    }

    if ($a->required) {
      $cart->address->$type->hidden = FALSE;
      // todo add dropdown or ?
    }
  }
}

function order_cart_summary() {
  if (!$cart = order_cart_load()) return;
  $cart = order_cart_prepare($cart);

  $count = count($cart->items);
  $total = 0;
  foreach ($cart->items as $nid => $data) {
    $total += $item->order_cost;
  }
  return array('count' => $count, 'total' => $total);
}

function order_cart_form($cart) {
  $form = array();
  $form['cart'] = array('#type' => 'value', '#value' => $cart);

  $form['items'] = array();
  foreach ($cart->items as $nid => $data) {
    $node = $data['node'];
    $form['items'][$nid] = array(
      'name-'. $nid => array(
        '#type'  => 'markup',
        '#value' => l($node->title, 'node/'. $node->nid),
      ),
      'description' => array(
        '#type'  => 'markup',
        '#value' => theme('order_cart_teaser', $node),
      ),
      'qty-'. $nid => array(
        '#type' => 'textfield',
        '#default_value' => $data['qty'],
        '#size' => 2,
        '#description' => l('remove', 'order/cart/update/'. $node->nid .'/0'),
      ),
      'cost-'. $nid => array(
        '#type' => 'markup',
        '#value' => $data['cost'],
      ),
    );
    if (isset($data['form'])) {
      foreach ($data['form'] as $key => $val) {
        $form['items'][$nid]['description'][$key] = $val;
      }
    }
  }
  if ($cart->items_extra) {
    $form['subtotal'] = array('#type' => 'markup', '#value' => $cart->costs['subtotal']);
    $form['items_extra'] = array();
    foreach ($cart->items_extra as $name => $data) {
    }
  }
  foreach ($cart->items_extra as $name => $data) {
    $form['items_extra'][$name] = array(
      'name-'. $name => array(
        '#type'  => 'markup',
        '#value' => $data['name'],
      ),
      'description' => array(
        '#type'  => 'markup',
        '#value' => $data['description'],
      ),
      'cost-'. $name => array(
        '#type' => 'markup',
        '#value' => $data['cost'],
      ),
    );
    if (isset($data['form'])) {
      foreach ($data['form'] as $key => $val) {
        $form['items_extra'][$name]['description'][$key] = $val;
      }
    }
  }
  $form['total'] = array('#type' => 'markup', '#value' => $cart->costs['total']);
  $form['update'] = array('#type' => 'submit', '#value' => t('Update'));
  $form['delete'] = array('#type' => 'submit', '#value' => t('Empty Cart'));
  if ($cart->valid) {
    $form['checkout'] = array("#type" => 'submit', '#value' => t('Checkout'));
  }

  $form['address'] = array();
  if (function_exists('postal_form')) {
    foreach ($cart->address as $type => $a) {
  //    if ($a->hidden) continue;
      $form['address'][$type] = postal_form($cart->address->$type, array(), $cart);
      $form['address'][$type]['#type'] = 'fieldset';
      $form['address'][$type]['#title'] = $a->title;
      $form['address'][$type]['#collapsible'] = TRUE;
      $form['address'][$type]['#collapsed'] = (!$a->required || !$cart->address->$type->empty);
    }
  }

  return $form;
}

function order_cart_form_submit($form, &$form_state) {
  $op = $_POST['op'];
  if ($op == t('Checkout')) {
    $form_state['redirect'] = 'order/cart/checkout';
    return;
  }

  $cart = order_cart_load();
  $fields = array('cart', 'update', 'checkout', 'delete', 'form_token', 'form_id');
  foreach ($values as $key => $val) {
    if ($key != ($nid = str_replace('qty-', '', $key))) {
      if (!isset($cart->items[$nid]) || $cart->items[$nid]['qty'] != $val) {
        $cart->items[$nid]['qty'] = $val;
        $cart->items[$nid]['stamp'] = time();
      }
    }
    elseif (!in_array($key, $fields)) {
      $cart->$key = $val;
    }
  }

  order_cart_load($cart);

  if ($op == t('Checkout')) $form_state['redirect'] = 'order/cart/checkout';

  $form_state['redirect'] = 'order/cart';
}

function order_postalapi($op, $user, &$address) {
  switch ($op) {

    case 'info':
      break;

    case 'settings':
      break;

    case 'list':
      $addresses = array();

      if (!$user->uid) return $addresses;

      $res = db_query("SELECT 'order' AS module, MAX(a.order_nid) AS aid,
        DISTINCT(name, street1, street2, zip, city, state, country)
        FROM {order_transaction_address} a
        LEFT JOIN {order_transaction} t ON a.order_nid = t.nid
        WHERE t.uid = %d ORDER BY a.order_nid DESC", $user->uid);

      while ($row = db_fetch_object($res)) {
        $addresses[] = $row;
      }
      return $addresses;

    case 'insert':
    case 'update':
    case 'delete':
      break;
  }
}

/*
 * Add to cart
 */
function order_cart_update($nid='', $qty=1) {
  $cart = order_cart_load();
  if ($qty == 0) {
    unset($cart->items[$nid]);
  }
  else {
    if (!isset($cart->items[$nid])) $cart->items[$nid] = array();
    $cart->items[$nid]['qty'] = $qty;
    $cart->items[$nid]['stamp'] = time();
  }
  order_cart_load($cart);
  drupal_goto('order/cart');
}

function order_cart_checkout($nid=NULL) {
  global $user;

  if ($out = order_cart_registration()) return $out;

  if (isset($nid)) {
    $order = node_load($nid);
  }

  elseif ($cart = order_cart_load()) {
    $order = order_cart_prepare($cart);
    $fields = array('cid', 'uid', 'sid', 'title', 'stamp', 'items', 'items_extra', 'address', 'valid', 'costs');

    if (!$order->valid) {
      drupal_set_message(t('There are errors with your cart'), 'error');
      drupal_goto('order/cart');
    }

    order_cart_delete($cart);

    $order->type = 'order';
    $order->status = 0;
    $order->promoted = 0;
    unset($order->stamp, $order->cid, $order->sid);

    node_save($order);
    $order = node_load($order->nid);
  }

  else {
    if ($user->uid && ($nid = db_result(db_query(db_rewrite_sql("SELECT nid
      FROM {node} WHERE type = 'order'
      AND uid = %d ORDER BY created DESC LIMIT 1", 'node'), $user->uid)))) {
      drupal_goto('order/cart/checkout/'. $nid);
    }
    else {
      drupal_set_message(t('Cart Error'), 'error');
    }
  }

  // now do checkout stuff:  need to pay? other api functions?
  $out = node_view($order, FALSE, TRUE);
  return $out;
}

function order_cart_confirm_delete() {
  return confirm_form(
    'order_cart_confirm_delete',
    array(),
    t('Are you sure you wish to clear your cart?'),
    'order/cart');
}

function order_cart_confirm_delete_submit() {
  $cart->delete();
  drupal_set_message(t('Your cart has been cleared.'));
}

function order_cart_registration() {
  global $user;
  // check which scenario the current user falls into
  if ($user->uid && user_access('place order')) {
    // user is logged and can place an order
    return;

  }

  if ($user->uid) {   // user is logged in but CAN'T place an order

    return drupal_access_denied();
  }

  if (!$user->uid && user_access('place order') && $_REQUEST['skiplogin']) {

    return;
  }

  // user is not logged in, but she has a cart associated with a uid
  // this would happen if she registered but hasn't logged in yet, so we're
  // going to run with it
  if ($cart = order_cart_load()) {
    if (($user->uid == 0) && $cart->extra['uid']) {
      return;
    }
  }

  // user is anonymous, we present the login and registration forms
  if (user_access('place order')) {
    $link = l(t('continue without registration'),
              'order/cart/checkout',
              NULL,
              'skiplogin=1');

  }
  $_REQUEST['destination'] = 'order/cart/checkout';

  return theme('order_login_page', $link);
}

function order_cart_alter($op, &$cart, &$item, &$data) {
  switch ($op) {
    case 'cart':
      return;
    case 'item':
      return;
    case 'total':
      return;
  }
}

/*  Implementation of order_cart_arguments()
 */
function order_cart_arguments($op) {
  $args = array();
  switch ($op) {
    case 'cart':
      $args['total'] = array(
        'name' => t('Cart Total'),
        'handler' => '',
        'help' => '',
      );
      return $args;
    case 'item':
      $args['cost'] = array(
        'name' => t('Price'),
        'handler' => '',
        'help' => '',
      );
      return $args;
  }
}
