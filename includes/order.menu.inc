<?php // $Id$

function order_menu_menu() {
  $path = drupal_get_path('module', 'order') .'/includes';
  return array(
    'order/cart' => array(
      'title' => 'Cart summary',
      'page callback' => 'order_cart',
      'access callback' => 'order_access',
      'file' => 'order.cart.inc',
      'file path' => $path,
    ),
    'order/cart/update' => array(
      'page callback' => 'order_cart_update',
      'access callback' => 'order_access',
      'type' => MENU_CALLBACK,
      'file' => 'order.cart.inc',
      'file path' => $path,
    ),
    'order/cart/delete' => array(
      'title' => 'Are you sure you wish to delete your cart?',
      'page callback' => 'order_cart_confirm_delete',
      'access callback' => 'order_access',
      'type' => MENU_CALLBACK,
      'file' => 'order.cart.inc',
      'file path' => $path,
    ),
    'order/cart/checkout' => array(
      'title' => 'Check out',
      'page callback' => 'order_cart_checkout',
      'access callback' => 'order_access',
      'file' => 'order.cart.inc',
      'file path' => $path,
    ),
    'order/%order' => array(
      'title' => 'View order',
      'type' => MENU_CALLBACK,
      'page callback' => 'theme',
      'page arguments' => array('order', 1),
      'access callback' => 'order_access',
      'access arguments' => array(1),
    ),
    'admin/order' => array(
      'path' => 'admin/order',
      'title' => 'Orders',
      'page callback' => 'order_admin',
      'description' => 'Order history and administration',
      'access arguments' => array('administer orders'),
      'file' => 'order.admin.inc',
      'file path' => $path,
    ),
    'admin/order/settings' => array(
      'title' => 'Order Settings',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('order_admin_settings'),
      'access arguments' => array('administer orders'),
      'description' => 'Configure your site order settings.',
      'file' => 'order.admin.inc',
      'file path' => $path,
    ),
    'admin/order/history' => array(
      'title' => 'Order history',
      'page callback' => 'order_admin_history',
      'access arguments' => array('administer orders'),
      'description' => 'Review and administrate this site\'s order history.',
      'file' => 'order.admin.inc',
      'file path' => $path,
    ),
    'admin/order/rules' => array(
      'title' => 'Order rules',
      'page callback' => 'order_admin_rules',
      'access arguments' => array('administer orders'),
      'description' => 'Construct order rules for your site.',
      'file' => 'order.admin.inc',
      'file path' => $path,
    ),
  );
}
