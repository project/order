<?php // $Id$

/**
 * Present a product editing form on the node form.
 */
function order_product_node_form($form_state, $form) {
  // TODO extend this someday, but for now use the built-in 'simple' handler.
  $handler = 'order_product';

  $form = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );

  $form[$opid]['handler'] = array(
  );
  $form[$opid]['weight'] = array(
  );
  $form[$opid]['title'] = array(
  );
  $form[$opid]['description'] = array(
  );
  $form[$opid]['description_format'] = array(
  );
  $form[$opid]['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow users to add this %type to an order', array('%type' => $node->type)),
    '#default_value' => $node->order,
  );
  $form[$opid]['cost'] = array(
    '#type' => 'textfield',
    '#title' => t('Cost'),
    '#size' => 9,
    '#default_value' => $node->order_cost,
  );
  return $form;
}
