<?php // $Id$

function order_admin() {
  // active shoppers
  // abandoned carts
  // list/manage/view orders

  $res = db_query("SELECT n.created, n.uid, u.name, o.nid, o.total, o.balance
    FROM {order_transaction} o
    LEFT JOIN {node} n ON o.nid = n.nid
    LEFT JOIN {users} u ON n.uid = u.uid
    ORDER BY n.created DESC");

  while ($row = db_fetch_object($res)) {
    $rows[] = array(
      format_date($row->created, 'short'),
      l($row->name, 'user/'. $row->uid),
      $row->total,
      $row->balance,
      l(t('view'), 'node/'. $row->nid),
    );
  }
  return theme('table', NULL, $rows);
}

function order_admin_rules() {
  $cart_arguments = module_invoke_all('order_cart_arguments', 'cart');
  $item_arguments = module_invoke_all('order_cart_arguments', 'item');
return ' ';
}

function order_admin_history() {
}

function order_admin_settings() {
  $form = array();

  $form['order_home'] = array(
    '#type' => 'textfield',
    '#multiple' => TRUE,
    '#title' => t('Order home page'),
    '#description' => t(''),
    '#default_value' => variable_get('order_home', ''),
  );

  $form['order_mail'] = array(
    '#type' => 'textfield',
    '#multiple' => TRUE,
    '#title' => t('E-Mail Address'),
    '#description' => t(''),
    '#default_value' => variable_get('order_mail', ''),
  );

  $types = node_get_types('names');
  unset($types['order']);

  $form['order_node_types'] = array(
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#title' => t('Node Types'),
    '#options' => $types,
    '#default_value' => variable_get('order_node_types', array()),
  );

  return system_settings_form($form);
}

/*
function _order_node_access($op, $node) {
  global $user;

  if ($op == 'create') {
   // only allow an administrator to create an order on-the-fly
    if ((arg(0) == 'node') && (arg(1) == 'add')) {
      return user_access('administer orders');
    }

    return user_access('place order');
  }

  if ($op == 'update') {
    if (($user->uid != 0) && ($node->uid == $user->uid)) {
      return user_access('place order');
    }

    return user_access('administer orders');
  }

  if ($op == 'delete') {
    return user_access('administer orders');
  }
}

function _order_node_save($node) {
  db_query("INSERT INTO {order_transaction} ( nid, total, balance )
    VALUES ( %d, %f, %f )", $node->nid, $node->costs['total'], $node->costs['total']);

  foreach ($node->items as $nid => $data) {
    $item = $data['node'];
    db_query("INSERT INTO {order_transaction_item}
      ( order_nid, type, vid, title, description, cost, qty )
      VALUES ( %d, '%s', %d, '%s', '%s', %f, %f )",
      $node->nid, 'item', $nid, $item->title, $item->body, $data['cost'], $data['qty'] );
  }

  if (is_array($node->items_extra)) {
    foreach ($node->items_extra as $name => $data) {
      db_query("INSERT INTO {order_transaction_item}
        ( order_nid, type, vid, title, description, cost, qty )
        VALUES ( %d, '%s', %d, '%s', '%s', %f, %f )",
        $node->nid, $name, NULL, $name, $data['body'], $data['cost'], $data['qty'] );
    }
  }

  foreach ($node->costs as $name => $cost) {
    db_query("INSERT INTO {order_transaction_cost} ( order_nid, name, cost )
      VALUES ( %d, '%s', %f )", $node->nid, $name, $cost);
  }

  foreach ($node->address as $type => $a) {
    db_query("INSERT INTO {order_transaction_address}
      ( order_nid, type, name, first_name, last_name, street1, street2, zip, city, state, country )
      VALUES ( %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' )",
      $node->nid, $type, $a->name, $a->first_name, $a->last_name, $a->street1, $a->street2, $a->zip, $a->city, $a->state, $a->country);
  }

  _order_set_status($node, 'order received');
}
*/

function _order_set_status($node, $status, $comment=NULL, $user=NULL, $ts=0) {
  $ts = $ts ? $ts : time();
  if (!$user) {
    global $user;
  }
  db_query("INSERT INTO {order_transaction_status}
    ( order_nid, uid, status, stamp, comment )
    VALUES ( %d, %d, '%s', %d, '%s' )",
    $node->nid, $user->uid, $status, $ts, $comment);
}
