<?php // $Id$

function order_theme_theme() {
  $path = drupal_get_path('module', 'order') .'/theme';

  return array(
    'order_cart_summary' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
    'order_cart_block' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
    'order' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
    'order_item_view' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
    'order_cart_form' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
    'order_cart_empty' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
    'order_cart_teaser' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
    'order_cart_links' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
    'order_login_page' => array(
      'file' => 'order.theme.inc',
      'path' => $path,
    ),
  );
}

function theme_order_cart_summary() {
  if (!$summary = order_cart_summary()) return;
  return t('%count items = %total', array('%count' => $count, '%total' => $total));
}

function theme_order_cart_block() {
  if (!$cart = order_cart()) return;
  return array('subject' => t('Your Cart'), 'contents' => $contents);
}

function theme_order($node) {
  drupal_add_css(drupal_get_path('module', 'order') .'/order.css');
  $out = '';
  foreach ($node->address as $type => $a) {
    $out .= '<div class = "order-postal order-postal-'. strtr($type, ' ', '-') .'">';
    $out .= '<h3>'. $type .'</h3>';
    $out .= theme('postal_address', $a) .'</div>';
  }
  $hdrs = array( t('Item'), t('Qty'), t('Price'), t('Total'));
  $rows = array();
  if (!is_array($node->items['item'])) return;
  foreach ($node->items['item'] as $nid => $data) {
    $rows[] = array(
      '<h4>'. l($data->title, 'node/'. $nid) .'</h4>',
      $data->qty,
      $data->cost,
      $data->cost * $data->qty,
    );
  }

  // see if any subtotal items exist and put them into a set of temporary
  // rows
  $tmprows = array();
  foreach ($node->items as $name => $data) {
    if ($name == 'item') continue;
    $data = $data[0];
    $tmprows[] = array(
      array('data' => $data->title, 'align' => 'right',
      'class' => 'order-cart-subtotal-item', 'colspan' => 3),
      array('data' => $data->cost, 'class' => 'order-subtotal-item-cost'),
    );
  }

  if (count($tmprows)) {
    // add the subtotal row
    $rows[] = array(
      array('data' => t('Subtotal:'), 'align' => 'right',
      'class' => 'order-cart-subtotal order-total-column', 'colspan' => 3),
      array('data' => $node->costs['subtotal'], 'class' => 'order-subtotal-cost order-cost-column'));
    // add the tmp rows to the overall table
    $rows = array_merge($rows, $tmprows);
  }
  $rows[] = array(
    array('data' => t('Total:'), 'align' => 'right',
    'class' => 'order-cart-total order-total-column', 'colspan' => 3),
    array('data' => $node->costs['total'], 'class' => 'order-total-cost order-cost-column'));

  $items = theme('table', $hdrs, $rows, array('class' => 'order-invoice-items'));

  $rows = array();
  foreach ($node->history as $data) {
    $rows[] = array(
      format_date($data->stamp, 'small'),
      $data->status,
      $data->comments,
    );
  }
  $history = theme('table', NULL, $rows, array('class' => 'order-invoice-history'));

  $out .= '<h2>'. t('Items') .'</h2>';
  $out .= $items;
  $out .= '<h2>'. t('Order History') .'</h2>';
  $out .= $history;

  return '<div class = "order-invoice">'. $out .'</div>';
}

function theme_order_item_view($node) {

  if (isset($node->order_parents)) {
  }
  if ($node->order) {
    $node->content['order']['#value'] = l(t('add to cart'), 'order/cart/update/'. $node->nid .'/1');
    $node->content['order']['#weight'] = 1;
  }
  return $node;
}

function theme_order_cart_form($form) {
  theme('add_style', drupal_get_path('module', 'order') .'/order.css');
  $cart = $form['cart']['#value'];

  $hdrs = array('', t('Qty'), t('Price'));

  foreach ($cart->items as $id => $item) {
    $rows[] = array(

      '<div class = "order-cart-item"<h3>'. drupal_render($form['items'][$id]['name-'. $id]) .'</h3>'. drupal_render($form['items'][$id]['description']) .'</div>',
      array('data' => drupal_render($form['items'][$id]['qty-'. $id]), 'class' => 'cart-quantity'),
      array('data' => drupal_render($form['items'][$id]['cost-'. $id]), 'class' => 'cart-price'),
    );
  }
  if ($cart->items_extra) {
    $rows[] = array(
      array('data' => t('Subtotal: ') . drupal_render($form['subtotal']), 'colspan' => 3, 'align' => 'right'));
  }

  foreach ($cart->items_extra as $id => $data) {
    $rows[] = array(
      array('data' => drupal_render($form['items_extra'][$id]['name-'. $id])
        . drupal_render($form['items_extra'][$id]['description']), 'class' => 'cart-extra-name', 'colspan' => 2),
      array('data' => drupal_render($form['items_extra'][$id]['cost-'. $id]), 'class' => 'cart-extra-price'),
    );
  }

  $rows[] = array(
    array('data' => t('Total: ') . drupal_render($form['total']), 'colspan' => 3, 'class' => 'cart-total')
  );

  $out = theme('table', $hdrs, $rows, array('id' => 'order_cart'));
  $out .= drupal_render($form);
  return $out;
}

function theme_order_cart_empty() {
  return t('You have no items in your cart');
}

function theme_order_cart_teaser($node) {
  if ($node->teaser) {
    return $output .'<p>'. $node->teaser .'</p>';
  }

  return $output .= node_view($node, TRUE, TRUE, FALSE);
}

function theme_order_cart_links($cart) {
  $links = array();
  $links[] = l(t('empty cart'), 'order/cart/delete');
  if ($path = variable_get('order_home', '')) {
    $links[] = l(t('continue shopping'), $path);
  }
  if ($cart->valid) {
    $links[] = l(t('check out'), 'order/cart/checkout');
  }

  $out = '<p>'. join(' ', $links) .'</p>';
  return $out;
}

function theme_order_login_page($skiplink = '') {
  drupal_add_css(drupal_get_path('module', 'order') .'/order.css');
  // TODO this should probably be replaced with a form pull from the login system. it can then be altered appropriately.
  $output = '';

  // login form

  $pass_link = l(t('you may request a new password sent to your email address'), 'user/password');
  $output .= '<div class="order-anon order-anon-login">';
  $output .= "<h2>Login</h2>\n";
  $output .= "<p>If you have already registered at this site, you may login now to complete your order. If you have forgotten your login name and password, $pass_link.</p>\n";

  $output .= drupal_get_form('user_login') .'</div>';

  if (variable_get('user_register', 1)) { // make sure users can create accounts
    // registration form
    $output .= '<div class="order-anon order-anon-register">';
    $output .= "<h2>Register a New Account</h2>\n";
    $output .= "<p>You may register a new account to complete your order. Your account information will confirmed by email. Your cart will be saved until you can log in.</p>";
    $output .=  drupal_get_form('user_register') .'</div>';
  }

  if ($skiplink) {
    $output  .= '<div class = "order-anon order-anon-skip">&raquo; You may also '. $skiplink .'</div>';
  }

  return $output;
}
